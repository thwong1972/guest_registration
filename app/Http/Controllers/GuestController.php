<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Branch;
use App\GuestLog;


class GuestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the guest form
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showGuestForm(Request $request)
    {
        $input = $request->all();
        $branch = Branch::where('hashedbranchname', $input['v'])->first();
        
        return view('guest.guest_registration',compact('branch'));
    } 
    

    public function saveGuestDetails(Request $request)
    {
        $this->validate($request, [
            'branchid' => 'required',
            'guest_name' => 'required',
            'documentid' => 'required',
            'phone' => 'required',
            'temperature' => 'required',
        ]);

        $guestlog = GuestLog::create($request->all());

        return view('thankyou');
    }      



    public function showGuestLog(Request $request)
    {
        if (!Auth::check()) { return redirect()->route('admin'); }

        $input = $request->all();
        $data = GuestLog::orderBy('created_at','DESC')->paginate(10);
        
        return view('guest.guestlog',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
        
    } 


    public function outputGuestLog()
    {
        $data = GuestLog::orderBy('created_at','DESC')->get();

        $filename = 'FullGuestRegistrationLog-' . date('Ymd') . '.csv';

        // setting the header
        $content = "";
        $content = $content . "Date Time,";
        $content = $content . "Branch Name,";
        $content = $content . "Guest Name,";
        $content = $content . "Document ID,";
        $content = $content . "Phone,";
        $content = $content . "Temperature,";
        $content = $content . "Email,";
        $content = $content . "Address";
        $content = $content . PHP_EOL;

        foreach ($data as $key => $guest) {
            $content = $content . $guest->created_at  . ',';
            $content = $content . $guest->branch->branch_name  . ',';
            $content = $content . $guest->guest_name . ',';
            $content = $content . $guest->documentid . ',';
            $content = $content . $guest->phone . ',';
            $content = $content . $guest->temperature . ',';
            $content = $content . $guest->email . ',';
            $content = $content . $guest->address;
            $content = $content . PHP_EOL;
        }

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/octet-stream; "); 
        header("Content-Transfer-Encoding: binary");

        echo chr(239) . chr(187) . chr(191) . $content;
        



    }

}
