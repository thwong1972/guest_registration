<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class GuestLog extends Model
{

	use Notifiable;

	protected $table = 'tbl_guest_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branchid',
        'guest_name',
        'documentid',
        'phone',
        'temperature',
        'email',
        'address',
    ];


    public function branch() {

        return $this->belongsTo('App\Branch', 'branchid', 'id');
    }

}
