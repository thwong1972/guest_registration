@extends('layouts.app')


@section('content')

        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Branch Maintenance</h1>
          
          @if ($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{ $message }}</p>
          </div>
          @endif


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a class="btn btn-success" href="{{ route('branches.create') }}">New Branch</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="80px">No</th>
                      <th>Branch Name</th>
                      <th width="280px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $key => $branch)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td>{{ $branch->branch_name }}</td>
                      <td>
                        <a class="btn btn-info" href="{{ route('branches.show',$branch->id) }}">Show</a>
                         <a class="btn btn-primary" href="{{ route('branches.edit',$branch->id) }}">Edit</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $data->render() !!}
              </div>
            </div>
          </div>

        </div>

@endsection