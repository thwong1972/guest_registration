@extends('layouts.app')


@section('content')

<div class="container-fluid">
    
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Branch Maintenance | Edit Branch</h1>



    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a class="btn btn-primary" href="{{ route('branches.index') }}"> Back</a>
        </div>
        <div class="card-body">

        @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
               @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
               @endforeach
            </ul>
          </div>
        @endif


            {!! Form::model($branch, ['method' => 'PATCH','route' => ['branches.update', $branch->id]]) !!}
            <div class="row">
                <div class="col-xs-3 col-sm-6 col-md-4">
                    <div class="form-group">
                        <strong>Branch Name:</strong>
                        {!! Form::text('branch_name', null, array('placeholder' => 'Branch Name','class' => 'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-4 text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection