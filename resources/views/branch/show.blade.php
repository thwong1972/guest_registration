@extends('layouts.app')


@section('content')

<div class="container-fluid">
    
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Branch Maintenance | Branch Details</h1>



    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a class="btn btn-primary" href="{{ route('branches.index') }}"> Back</a>
        </div>
        
        <div class="card-body">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Branch Name:</strong>
                        {{ $branch->branch_name }}
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>URL for Customer Registration:</strong><br>
                        {!! url("/guest_registration?v=" . $branch->hashedbranchname) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>QR Code for Customer Registration:</strong><br>
                        {!! QrCode::size(300)->generate(url("/guest_registration?v=" . $branch->hashedbranchname)); !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection