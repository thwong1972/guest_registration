<html>
<title>{{ env("ENTITY", "Company") }} | Guest Registration</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>

<div class="card">
<article class="card-body mx-auto" style="max-width: 350px; min-width: 350px;">

    <div class="card-title mt-3 text-center"><img src="img\logo-en.png"></div>
    <h4 class="card-title mt-3 text-center">Visitor Registration</h4>
    <h5 class="card-title mt-3 text-center"> {{ $branch->branch_name }}</h5>
    {!! Form::open(array('route' => 'save_guest_registration','method'=>'POST')) !!}

        <input name="branchid" class="form-control" placeholder="Full name" type="hidden" required="true" value="{{ $branch->id }}">

        <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"> <i class="fa fa-user"></i> </span>
             </div>
            <input name="guest_name" class="form-control" placeholder="Full name" type="text" required="true">
        </div> <!-- form-group// -->
        <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"> <i class="fa fa-address-card"></i> </span>
             </div>
            <input name="documentid" class="form-control" placeholder="Personal ID or passport number" type="text" required="true">
        </div> <!-- form-group// -->    
        <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
            </div>
            <input name="phone" class="form-control" placeholder="Phone number" type="text" required="true">
        </div> <!-- form-group// -->
        <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"> <i class="fa fa-user-circle"></i> </span>
            </div>
            <input name="temperature" class="form-control" placeholder="Temperature" type="number" step=".1" required="true">
        </div> <!-- form-group// -->
        
        <div class="form-group input-group">
        Optional
        </div> <!-- form-group// -->   

        <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
             </div>
            <input name="email" class="form-control" placeholder="Email address" type="email">
        </div> <!-- form-group// -->                                     
        <div class="form-group input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"> <i class="fa fa-address-book"></i> </span>
             </div>
            <textarea name="address" class="form-control" placeholder="Enter residential address" style="height:100px;"></textarea>
        </div> <!-- form-group// -->                                     
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block"> Register </button>
        </div> <!-- form-group// -->      

    {!! Form::close() !!}
</article>
</div> <!-- card.// -->

</body>
</html>