@extends('layouts.app')


@section('content')

        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Guest Registration Log</h1>
          
          @if ($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{ $message }}</p>
          </div>
          @endif


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3 d-sm-flex align-items-center justify-content-between mb-4">
              <a class="btn btn-primary" href="{{ route('admin') }}"> Back</a>
                          <a href="{{ route('export_guest_registration_log') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Branch Name</th>
                      <th>Guest Name</th>
                      <th>Document ID</th>
                      <th>Phone</th>
                      <th>Temperature</th>
                      <th>Email</th>
                      <th>Address</th>
                      <th>Date Time</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $key => $guest)
                    <tr>
                      <td>{{ $guest->branch->branch_name }}</td>
                      <td>{{ $guest->guest_name }}</td>
                      <td>{{ $guest->documentid }}</td>
                      <td>{{ $guest->phone }}</td>
                      <td>{{ $guest->temperature }}</td>
                      <td>{{ $guest->email }}</td>
                      <td>{{ $guest->address }}</td>
                      <td>{{ $guest->created_at }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $data->render() !!}
              </div>
            </div>
          </div>

        </div>

@endsection