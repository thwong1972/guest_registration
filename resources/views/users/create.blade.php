@extends('layouts.app')


@section('content')

<div class="container-fluid">
    
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">User Maintenance | New User</h1>



    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
        </div>
        <div class="card-body">

        @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
               @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
               @endforeach
            </ul>
          </div>
        @endif



    {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
    <div class="row">
                <div class="col-xs-3 col-sm-6 col-md-4">
                    <div class="form-group">
                        <strong>Name:</strong>
                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            </div>
                </div>
                <div class="col-xs-3 col-sm-6 col-md-4">
                    <div class="form-group">
                        <strong>Email:</strong>
                {!! Form::email('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3 col-sm-6 col-md-4">
                    <div class="form-group">
                        <strong>Password:</strong>
                {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
            </div>
                </div>
                <div class="col-xs-3 col-sm-6 col-md-4">
                    <div class="form-group">
                        <strong>Confirm Password:</strong>
                {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
            </div>
                </div>
                <div class="col-xs-6 col-sm-8 col-md-8 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
    {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection