@extends('layouts.app')


@section('content')

        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">User Maintenance</h1>
          
          @if ($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{ $message }}</p>
          </div>
          @endif


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a class="btn btn-success" href="{{ route('users.create') }}">New User</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="80px">No</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th width="280px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $key => $user)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>
                        <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                         <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                          @if ($user->id != 1)
                          {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                          {!! Form::close() !!}
                          @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $data->render() !!}
              </div>
            </div>
          </div>

        </div>

@endsection