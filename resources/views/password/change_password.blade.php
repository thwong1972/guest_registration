@extends('layouts.app')


@section('content')

<div class="container-fluid">
    
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Change Password</h1>



    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a class="btn btn-primary" href="{{ route('admin') }}"> Back</a>
        </div>
        <div class="card-body">


    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
        </ul>
      </div>
    @endif

    @if ($message = Session::get('success'))
          <div class="alert alert-success">
            <p>{{ $message }}</p>
          </div>
    @endif

    @if ($message = Session::get('error'))
          <div class="alert alert-danger">
            <p>{{ $message }}</p>
          </div>
    @endif

        {!! Form::model($user, ['method' => 'POST','route' => ['update_password']]) !!}
        <div class="row">
            <div class="col-xs-3 col-sm-6 col-md-4">
                <div class="form-group">
                    <strong>Current Password:</strong>
                    {!! Form::password('current_password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3 col-sm-6 col-md-4">
                <div class="form-group">
                    <strong>New Password:</strong>
                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3 col-sm-6 col-md-4">
                <div class="form-group">
                    <strong>Confirm Password:</strong>
                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 text-right">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>    
        {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection