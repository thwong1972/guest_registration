<?php

use Illuminate\Support\Facades\Route;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if (User::all()->count() == 0) return redirect('/register'); else return redirect('/admin'); 
    return view('welcome');
});

Auth::routes(['register' => false]);

//Admin
Route::group(['middleware' => ['auth']], function() {
    Route::resource('users','UserController');
    Route::resource('branches','BranchController');
});

Route::get('/register', function () {
	if (User::all()->count() == 0) return App::call('App\Http\Controllers\Auth\RegisterController@showRegistrationForm');
})->name('register');

Route::post('/register', 'Auth\RegisterController@register');

Route::get('/admin', 'HomeController@index')->name('admin');
Route::post('/guest_registration', 'GuestController@saveGuestDetails')->name('save_guest_registration');
Route::get('/guest_registration', 'GuestController@showGuestForm')->name('guest_registration');
Route::get('/guestlog', 'GuestController@showGuestLog')->name('guest_registration_log');
Route::get('/guestlogfile', 'GuestController@outputGuestLog')->name('export_guest_registration_log');

Route::get('/change_password', 'UserController@ShowChangePassword')->name('show_change_password');
Route::post('/change_password', 'UserController@SavePassword')->name('update_password');
