## About Guest Registration

Simple guest registration application to assist in Covid19 tracking.

- Support multi branch guest registration
- Guest may use QR Code to scan for guest registration link
- Guest registration report
- Multi users


## Pre-requisite
- Apache 2.4 and above
- PHP 7.2.5 and above
- MySQL 5.6 and above


## PHP required modules
- PHP >= 7.2.5
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension


## License

Guest Registraiton is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
